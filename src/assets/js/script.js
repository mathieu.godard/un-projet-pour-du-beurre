const cards = document.querySelector('.cards');
const template = document.getElementById('list');

fetch(`https://data.rennesmetropole.fr/api/records/1.0/search/?dataset=localisation-et-etat-des-projets-du-budget-participatif&q=&facet=quartier&facet=realise&facet=annee_budget`)
.then(res => res.json())
.then(projects => {
    for (let i = 0; i < projects.records.length; i++) {
        const { realise, libelle, description, quartier, annee_budget } = projects.records[i].fields;
        let { photo } = projects.records[i].fields;
        if (projects.records[i].fields.photo === undefined) {
            photo = "no-image-project.png";
        }
        displayProject(realise, photo, libelle, description, quartier, annee_budget);
    }
})
.catch(error => console.log(error))

function displayProject(realise, photo, libelle, description, quartier, annee_budget) {
    const clone = template.content.cloneNode(true);
    let result;
    realise !== "Réalisé" ? result = "Refusé 🖕" : result = "Accepté 🤘"
    clone.querySelector(".realised").innerText = result;
    clone.querySelector("article img").setAttribute("src", photo);
    clone.querySelector("h2").innerText = libelle;
    clone.querySelector(".description").innerText = description;
    clone.querySelector(".district").innerText = quartier;
    clone.querySelector(".year").innerText = annee_budget;
    cards.appendChild(clone);
}