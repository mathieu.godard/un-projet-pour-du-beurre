# Afficher les projets du budget participatif de la ville de Rennes
API disponible sur [https://data.rennesmetropole.fr](https://data.rennesmetropole.fr/explore/dataset/localisation-et-etat-des-projets-du-budget-participatif/api/)

# Cloner le projet

```bash
$ git clone git@gitlab.com:mathieu.godard/un-projet-pour-du-beurre.git
```

# Lancer le projet

```bash
$ npm install
```
puis
```bash
$ npm start
```

...et en avant Guingamp !